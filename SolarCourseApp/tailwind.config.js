/** @type {import('tailwindcss').Config} */
const defaultTheme = require('tailwindcss/defaultTheme')

module.exports = {
  content: [
    "./src/**/*.{html,ts}",
  ],
  corePlugins: {
    preflight: false,
  },
  theme: {
    extend: {
      colors: {
        'primary': '#FF8261',
        'gray': '#e2e2e2',
        'success': '#52C41A',
        'error': '#FF4D4F',
        'warning': '#FAAD14'
      },
    },
  },
  plugins: [],
}
