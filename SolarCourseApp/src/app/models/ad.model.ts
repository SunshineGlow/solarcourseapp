export interface AdGetPageItem {
  id: string;
  name: string;
  photo: string;
  price: number;
  locationText: string;
  createdAt: string;
  ownerId: string;
  status: string;
}
