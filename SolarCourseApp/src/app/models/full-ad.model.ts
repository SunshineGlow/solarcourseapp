import { FullAdPhotoItem } from './full-ad-photo-Item';

export interface FullAd {
  id: string;
  name: string;
  locationText: string;
  price: number;
  photos: FullAdPhotoItem[];
  ownerName: string;
  ownerNumber: string;
  ownerPhoto: string;
  text: string;
  status: string;
  moderationComment: string;
  ownerId: string;
}
