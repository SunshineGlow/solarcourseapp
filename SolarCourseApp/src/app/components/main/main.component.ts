import { Component, OnInit } from '@angular/core';
import {AdService} from "../../services/ad.service";
import {BehaviorSubject, Subject} from "rxjs";

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  public ads: any;
  public isLoading$: BehaviorSubject<any> = new BehaviorSubject<any>(true);

  public pageSize = 1;
  public pageNumber = 1;

  constructor(public adService: AdService) {
  }

  ngOnInit(): void {
    this.adService.getPagedFiltered(0, this.pageSize).subscribe(res => {
      this.ads = res;
      this.isLoading$.next(false);
    })
  }

  onChange(index: number) {
    this.pageNumber = index;

    this.isLoading$.next(true);
    this.adService.getPagedFiltered((index-1)*this.pageSize, this.pageSize).subscribe(res => {
      this.ads = res;
      this.isLoading$.next(false);
    })
  }
}
