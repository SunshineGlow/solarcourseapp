import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import {map, Observable} from 'rxjs';
import { Category } from '../models/category.model';

@Injectable({
  providedIn: 'root',
})
export class CategoryService {
  private adUrl = `${environment.apiUrl}/categories`;

  constructor(private http: HttpClient) {}

  public getAll(): Observable<any> {
    return this.http.get<any>(`${this.adUrl}/getAll`).pipe(map(categories => {
      return this.arrayToNzCascade(categories.items);
    }));
  }

  private arrayToNzCascade(array: Category[]) {
    return array.map(item => {
      const processed =  {
        key: item.id,
        title: item.name,
      } as any;

      if (item.childCategories?.length) {
        processed.children = this.arrayToNzCascade(item.childCategories);
      } else {
        processed.isLeaf = true;
      }
      return processed;
    })
  }
}
