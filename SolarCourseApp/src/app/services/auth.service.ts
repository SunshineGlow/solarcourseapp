import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {BehaviorSubject, EMPTY, Observable, shareReplay, switchMap, tap} from 'rxjs';
import { Router } from '@angular/router';
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  public id$ = new BehaviorSubject<string | null>(this.id);
  public user$ = this.id$.pipe(switchMap(id => this.getSelfById(id)));

  get token(): string | null {
    return localStorage.getItem('token');
  }

  set token(value: string | null) {
    if (!value) {
      localStorage.removeItem('token');
      return;
    }
    localStorage.setItem('token', value);
  }
  get id(): string | null {
    return localStorage.getItem('id');
  }

  set id(value: string | null) {
    this.id$.next(value);
    if (!value) {
      localStorage.removeItem('id');
      return;
    }
    localStorage.setItem('id', value);
  }

  constructor(private httpClient: HttpClient, private router: Router) {
  }

  register(model: any): Observable<any> {
    // Отправляем post запрос на url, ошибки обрабатываются через ErrorInterceptor
    return this.httpClient.post(`${environment.apiUrl}/users/register`, model);
  }

  login(model: any): Observable<any> {
    // Отправляем post запрос на url, в ответ нам приходит модель {
    //                                                              id: string,
    //                                                              token: string }
    //
    // Следовательно записываем токен в localStorage для отправки запросов с токеном. См AuthInterceptor, ошибки обрабатываются через ErrorInterceptor
    return this.httpClient
      .post(`${environment.apiUrl}/users/login`, model)
      .pipe(
        tap((res: any) => {
          this.token = res.token;
          this.id = res.id;
        })
      );
  }

  getSelfById(id: string | null): Observable<any> {
    if (!id) {
      return EMPTY;
    }
    return this.httpClient
      .get(`${environment.apiUrl}/users/getById`, {
        params: {
          id
        }
      });
  }

  logout(): void {
    this.token = null;
    this.id = null;
    window.location.reload();
  }
}
