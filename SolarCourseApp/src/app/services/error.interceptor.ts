import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
} from '@angular/common/http';
import { catchError, Observable, throwError } from 'rxjs';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { Router } from '@angular/router';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
  constructor(
    private nzNotificationService: NzNotificationService,
    private router: Router
  ) {}

  intercept(
    request: HttpRequest<unknown>,
    next: HttpHandler
  ): Observable<HttpEvent<unknown>> {
    // После отправки запроса ловим ошибки через rxJs catchError
    return next.handle(request).pipe(
      catchError((err) => {
        // При ошибках Not Authorized - показываем сообщение, отправляем на страницу входа
        if (err.status === 401 || err.status === 403) {
          this.nzNotificationService.error('Ошибка', 'Вы не авторизованы');
          this.router.navigateByUrl('/login');
        } else if (err.status === 400 || err.status === 500) {
          // При ошибках сервера/валидации - показываем сообщение об ошибке (структура объекта err.error может отличаться в зависимости что вы возвращаете на бэке)
          this.nzNotificationService.error('Ошибка', err.error.message);
        }
        return throwError(err);
      })
    );
  }
}
